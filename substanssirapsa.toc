\select@language {finnish}
\contentsline {section}{\numberline {1}Johdanto}{2}{section.1}
\contentsline {subsection}{\numberline {1.1}3D-tuloste}{2}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Tuotantoprosessi}{2}{subsection.1.2}
\contentsline {section}{\numberline {2}Verrokkina k\IeC {\"a}ytetyt dokumentit}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Antti M\IeC {\"a}kitien kallomallit}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Elinkasvatusalustat}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Johtop\IeC {\"a}\IeC {\"a}t\IeC {\"o}kset}{3}{subsection.2.3}
\contentsline {section}{\numberline {3}Haastattelut}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Ensimm\IeC {\"a}inen haastattelukierros}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Kaikkien osapuolten kokoontuminen}{4}{subsection.3.2}
\contentsline {section}{\numberline {4}Kannattavuuslaskelmat}{4}{section.4}
\contentsline {subsection}{\numberline {4.1}3D-tulostettu \IeC {\"a}\IeC {\"a}nt\IeC {\"o}v\IeC {\"a}yl\IeC {\"a} leikkausapuna}{4}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Uniapneasimulaatio}{5}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Opetusk\IeC {\"a}ytt\IeC {\"o}}{6}{subsection.4.3}
\contentsline {section}{\numberline {5}Uniapneasimulaattori}{6}{section.5}
\contentsline {subsection}{\numberline {5.1}Prototyyppi}{7}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Ansaintamalli}{7}{subsection.5.2}
\contentsline {section}{\numberline {6}\IeC {\"A}\IeC {\"a}nt\IeC {\"o}v\IeC {\"a}yl\IeC {\"a} opetusv\IeC {\"a}lineen\IeC {\"a}}{7}{section.6}
\contentsline {subsection}{\numberline {6.1}Ansaintamalli}{8}{subsection.6.1}
\contentsline {section}{\numberline {7}Tuntiseuranta projekteittain}{9}{section.7}
\contentsline {section}{Viitteet}{11}{section*.7}
